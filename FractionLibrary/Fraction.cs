﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FractionLibrary
{
    public class Fraction {

        private int numerator;
        private int denominator;
        private bool isNegative = false;

        public int Numerator {
            get => numerator;
            set {
                if (value < 0 && denominator < 0) {
                    numerator = -value;
                    denominator = -denominator;
                } else if (value < 0 && denominator >= 0) {
                    numerator = -value;
                    isNegative = isNegative ? false : true;
                } else if (value >= 0 && denominator < 0){
                    numerator = value;
                    isNegative = isNegative ? false : true;
                } else {
                    numerator = value;
                }
            }
        }

        public int Denominator {
            get => denominator;
            set {
                if (value < 0 && numerator < 0) {
                    denominator = -value;
                    numerator = -numerator;
                } else if (value < 0 && numerator >= 0) {
                    denominator = -value;
                    isNegative = isNegative ? false : true;
                } else if (value >= 0 && numerator < 0) {
                    denominator = value;
                    isNegative = isNegative ? false : true;
                } else {
                    denominator = value;
                }
            }
        }

        public bool IsNegative {
            get => isNegative;
            set {
                if (isNegative && value) {
                    isNegative = false;
                } else if (value && numerator < 0 && denominator > 0) {
                    numerator = -numerator;
                    isNegative = false;
                } else if (value && denominator < 0 && numerator > 0) {
                    denominator = -denominator;
                    isNegative = false;
                } else {
                    isNegative = value;
                }
            }
        }

        private Fraction() { }

        public Fraction(int numerator, int denominator) : this(numerator, denominator, false) { }

        public Fraction(int numerator, int denominator, bool isNegative) {
            this.IsNegative = isNegative;
            this.Numerator = numerator;
            this.Denominator = denominator;
        }

        public Fraction(Fraction it) : this(it.Numerator, it.Denominator, it.IsNegative) { }

        public Fraction(Fraction it, bool isNegative) : this(it.Numerator, it.Denominator, isNegative) { }

        public Fraction reduction() {
            if (numerator == 0) return this;

            int factor = numerator;
            int remainderNumerator = -1;
            int remainderDenominator = -1;

            while (factor > 1) {
                remainderNumerator = numerator % factor;
                remainderDenominator = denominator % factor;

                if (remainderNumerator == 0 && remainderDenominator == 0) break;
                factor--;
            }

            Numerator /= factor;
            Denominator /= factor;

            return this;
        }

        public double ToDouble() => (double) this;
        public float ToFloat() => (float) this;
        public long ToLong() => (long) this;
        public int ToInt() => (int) this;

        public override string ToString() {
            string value = $"{numerator}/{denominator}";
            return isNegative ? $"-({value})" : value;
        }

        public override bool Equals(object obj) => obj is Fraction ? this == (obj as Fraction) : false;
        public override int GetHashCode() => ToDouble().GetHashCode();

        public static bool operator >(Fraction left, Fraction right) => (double)left > (double)right;
        public static bool operator <(Fraction left, Fraction right) => (double)left < (double)right;
        public static bool operator ==(Fraction left, Fraction right) => (double)left == (double)right;
        public static bool operator !=(Fraction left, Fraction right) => (double)left != (double)right;
        public static bool operator >=(Fraction left, Fraction right) => (double)left >= (double)right;
        public static bool operator <=(Fraction left, Fraction right) => (double)left <= (double)right;

        public static Fraction operator +(Fraction left) => new Fraction(left);

        public static Fraction operator +(Fraction left, Fraction right) {
            return left.Numerator == 0 ? new Fraction(right)
                : right.Numerator == 0 ? new Fraction(left)
                : addOrSubtract(left, right, (a, b) => a + b);
        }

        public static Fraction operator +(Fraction left, int right) {
            return left + new Fraction(right * left.Denominator, left.Denominator);
        }

        public static Fraction operator -(Fraction left) => new Fraction(left, !left.isNegative);

        public static Fraction operator -(Fraction left, Fraction right) {
            return left.Numerator == 0 ? new Fraction(right, true)
                : right.Numerator == 0 ? new Fraction(left)
                : addOrSubtract(left, right, (a, b) => a - b);
        }

        public static Fraction operator -(Fraction left, int right){
            return left - new Fraction(right * left.Denominator, left.Denominator);
        }

        public static Fraction operator *(Fraction left, Fraction right) {
            return left.Numerator != 0
                ? new Fraction(left.Numerator * right.Numerator, left.Denominator * right.Denominator)
                : new Fraction(left);
        }

        public static Fraction operator *(Fraction left, int right){
            return left * new Fraction(right * left.Denominator, left.Denominator);
        }

        public static Fraction operator /(Fraction left, Fraction right) {
            return left.Numerator == 0 ? new Fraction(left)
                : right.Numerator == 0 ? throw new System.DivideByZeroException()
                : left * new Fraction(right.Denominator, right.Numerator);
        }
        public static Fraction operator /(Fraction left, int right) {
            return left / new Fraction(right * left.Denominator, left.Denominator);
        }

        public static explicit operator double(Fraction v) {
            return v.Numerator == 0 ? 0
                : v.IsNegative ? -((double)v.Numerator / (double)v.Denominator)
                : ((double)v.Numerator / (double)v.Denominator);
        }

        public static explicit operator float(Fraction v) {
            return v.Numerator == 0 ? 0
                : v.IsNegative ? -((float)v.Numerator / (float)v.Denominator)
                : ((float)v.Numerator / (float)v.Denominator);
        }

        public static explicit operator long(Fraction v) {
            return v.Numerator == 0 ? 0
                : v.IsNegative ? -((long)v.Numerator / (long)v.Denominator)
                : ((long)v.Numerator / (long)v.Denominator);
        }

        public static explicit operator int(Fraction v) {
            return v.Numerator == 0 ? 0
                : v.IsNegative ? -(v.Numerator / v.Denominator)
                : (v.Numerator / v.Denominator);
        }

        private static Fraction addOrSubtract(Fraction left, Fraction right, Func<int, int, int> action){
            int commonDenominator = lowestCommonDenominator(left.Denominator, right.Denominator);
            left.Numerator *= commonDenominator / left.Denominator;
            right.Numerator *= commonDenominator / right.Denominator;
            int leftNumerator = left.IsNegative ? -left.numerator : left.Numerator;
            int rightNumerator = right.IsNegative ? -right.numerator : right.Numerator;
            return new Fraction(action(leftNumerator, rightNumerator), commonDenominator);
        }

        public static int lowestCommonDenominator(int left, int right) {
            if (left == right) return left;

            bool leftOneIsMore = left > right;
            int l = left;
            int r = right;
            int factorSmaller = 2;
            int factorLarger = 1;

            do {
                if (leftOneIsMore) r = right * factorSmaller; else l = left * factorSmaller;

                factorSmaller++;

                if (leftOneIsMore ? r <= l : l <= r) continue;

                factorLarger++;
                if (leftOneIsMore) l = left * factorLarger; else r = right * factorLarger;

            } while (l != r);
            return leftOneIsMore ? l : r;
        }
    }
}
