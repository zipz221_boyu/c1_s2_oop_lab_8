﻿using FractionLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FractionConsole {
    internal class Program {

        private const ConsoleColor defTextColor = ConsoleColor.White;
        private const ConsoleColor errorColor = ConsoleColor.Red;
        private const ConsoleColor titleColor = ConsoleColor.Green;
        private const ConsoleColor hintColor = ConsoleColor.Yellow;

        static void Main(string[] args) {
            Console.OutputEncoding = Encoding.Unicode;
            Console.InputEncoding = Encoding.Unicode;

            Console.Title = "Лабораторна робота No8. Завдання 1";
            Console.ForegroundColor = defTextColor;
            Fraction it = createdFraction();

            Console.Clear();
            start(ref it);
        }

        private static void start(ref Fraction item) {
            printMenuTitle($"операції з дробом {item}");

            bool isConsoleClear = true;
            int select = selectMenuItem(new string[] {
                "Вихід",
                "змінити дріб",
                "змінити знак дробу на протилежний",
                "скоротити дріб",
                "арифметичні операції",
                "операції порівняння",
                "перетворення в примітивний тип даних"
            });

            switch (select) {
                case 0:
                    break;
                case 1:
                    changeFraction(ref item);
                    goto default;
                case 2:
                    item = -item;
                    goto default;
                case 3:
                    Console.Clear();
                    isConsoleClear = false;
                    writeLine($"{item.ToString()} -> {item.reduction()}");
                    goto default;
                case 4:
                    arithmeticOperations(ref item);
                    goto default;
                case 5:
                    comparisonOperations(item, createdFraction("дріб, з яким буде проводитися порівняння"));
                    goto default;
                case 6:
                    conversionToPrimitiveDataType(item);
                    goto default;
                default:
                    if (isConsoleClear) Console.Clear();
                    start(ref item);
                    break;
            }  
        }

        private static Fraction createdFraction(string title = "Створення дробу") {
            if (!string.IsNullOrEmpty(title)) printTitle(title, 40, hintColor);
            Fraction it = new Fraction(inputInt("чисельник"), inputInt("знаменник", false));
            writeLine("Вкажіть знак дробу");
            it.IsNegative = selectMenuItem(new string[] { "плюс", "мінус" }) == 1;
            printTitle("", 40, hintColor);
            return it;
        }

        private static void changeFraction(ref Fraction item) {
            printMenuTitle($"зміна дробу {item}");
            switch (selectMenuItem(new string[] { "назад", "змінити існуючий", "створити новий" })) {
                case 0:
                    break;
                case 1:
                    changeExistingFraction(ref item);
                    goto default;
                case 2:
                    item = createdFraction();
                    break;
                default:
                    changeFraction(ref item);
                    break;
            }
        }

        private static void changeExistingFraction(ref Fraction item) {
            printMenuTitle($"оновлення дробу {item}");
            int select = selectMenuItem(new string[] {
                "назад",
                "змінити чисельник",
                "змінити знаменник",
                "змінити знак на протилежний"
            });
            switch (select) {
                case 0:
                    break;
                case 1:
                    item.Numerator = inputInt("чисельник");
                    goto default;
                case 2:
                    item.Denominator = inputInt("знаменник", false);
                    goto default;
                case 3:
                    item = -item;
                    goto default;
                default:
                    changeExistingFraction(ref item);
                    break;
            }
        }

        private static void arithmeticOperations(ref Fraction item) {
            printMenuTitle($"арифметичні операції з дробом {item}");

            string result;
            bool isInt;
            int select = selectMenuItem(new string[] {
                "назад",
                "додавання",
                "віднімання",
                "множення",
                "ділення"
            });

            switch (select) {
                case 0:
                    break;
                case 1:
                    result = $"{item} + ";
                    isInt = isIntOperation("додати");
                    if (isInt) {
                        int it = inputInt("доданок");
                        result += it;
                        item += it;
                    } else {
                        Fraction it = createdFraction("доданок");
                        result += it;
                        item += it;
                    }
                    writeLine($"{result} = {item}");
                    goto default;
                case 2:
                    result = $"{item} - ";
                    isInt = isIntOperation("відняти");
                    if (isInt) {
                        int it = inputInt("від'ємник");
                        result += it;
                        item -= it;
                    } else {
                        Fraction it = createdFraction("від'ємник");
                        result += it;
                        item -= it;
                    }
                    writeLine($"{result} = {item}");
                    goto default;
                case 3:
                    result = $"{item} * ";
                    isInt = isIntOperation("помножити на");
                    if (isInt) {
                        int it = inputInt("множник");
                        result += it;
                        item *= it;
                    } else {
                        Fraction it = createdFraction("множник");
                        result += it;
                        item *= it;
                    }
                    writeLine($"{result} = {item}");
                    goto default;
                case 4:
                    result = $"{item} / ";
                    isInt = isIntOperation("помножити на");
                    if (isInt) {
                        int it = inputInt("множник");
                        result += it;
                        item /= it;
                    } else {
                        Fraction it = createdFraction("множник");
                        result += it;
                        item /= it;
                    }
                    writeLine($"{result} = {item}");
                    goto default;
                default:
                    arithmeticOperations(ref item);
                    break;
            }
        }

        private static void comparisonOperations(Fraction first, Fraction second) {
            printMenuTitle("операції порівняння дробів");

            int select = selectMenuItem(new string[] {
                "назад",
                $"{first} більше {second}",
                $"{first} більше або дорівнює {second}",
                $"{first} менше {second}",
                $"{first} менше або дорівнює {second}",
                $"{first} дорівнює {second}",
                $"{first} не дорівнює {second}"
            });

            switch (select) {
                case 0:
                    break;
                case 1:
                    writeLine(first > second ? "ТАК" : "НІ");
                    goto default;
                case 2:
                    writeLine(first >= second ? "ТАК" : "НІ");
                    goto default;
                case 3:
                    writeLine(first < second ? "ТАК" : "НІ");
                    goto default;
                case 4:
                    writeLine(first <= second ? "ТАК" : "НІ");
                    goto default;
                case 5:
                    writeLine(first == second ? "ТАК" : "НІ");
                    goto default;
                case 6:
                    writeLine(first != second ? "ТАК" : "НІ");
                    goto default;
                default: 
                    comparisonOperations(first, second);
                    break;
            }
        }

        private static void conversionToPrimitiveDataType(Fraction item) {
            printMenuTitle($"перетворення {item} в примітивний тип даних");
            int select = selectMenuItem(new string[] {
                "назад",
                "в int",
                "в long",
                "в float",
                "в double",
            });
            switch (select) {
                case 0:
                    break;
                case 1:
                    writeLine($"\n{item} -> {(int)item}");
                    goto default;
                case 2:
                    writeLine($"\n{item} -> {(long)item}");
                    goto default;
                case 3:
                    writeLine($"\n{item} -> {(float)item}");
                    goto default;
                case 4:
                    writeLine($"\n{item} -> {(double)item}");
                    goto default;
                default:
                    conversionToPrimitiveDataType(item);
                    break;
            }
        }

        private static bool isIntOperation(string operation) {
            writeLine($"{operation} ціле число чи дріб?");
            return selectMenuItem(new string[] { "дріб", "ціле число" }) == 1;
        }

        private static int inputInt(string name, bool canEnterZero = true) {
            write($"Вкажіть {name} та натисніть Enter: ");
            string tryAgain = ". Будь ласка, повторіть спробу!!!";

            string value = Console.ReadLine();

            if (string.IsNullOrEmpty(value)) {
                writeLine($"\n{name} не може бути порожнім{tryAgain}", errorColor);
                return inputInt(name, canEnterZero);
            }

            try {
                int n = int.Parse(value);

                if (!canEnterZero && n == 0) writeLine($"\n{name} не може дорівнювати нулю{tryAgain}", errorColor);
                else return n;

                return inputInt(name, canEnterZero);
            } catch (Exception e) {
                writeLine();
                writeLine(
                    (e is System.FormatException ? "Помилка формату введення занчення" : "Сталася невідома помилка") + tryAgain,
                    errorColor
                );
                return inputInt(name, canEnterZero);
            }
        }

        private static int selectMenuItem(string[] items, int def = -1) {
            if (items == null || items.Length == 0) return def;

            writeLine("\nВкажіть номер пункту та натисніть Enter");

            for (int i = 1; i < items.Length; i++) printMenuItem(i, items[i]);
            printMenuItem(0, items[0]);
            try {
                int it = int.Parse(Console.ReadLine());
                if (it >= 0 && it < items.Length) return it;
                throw new System.FormatException();
            } catch (Exception) {
                writeLine("\nЗробіть свій вибір!!!", errorColor);
            }

            return selectMenuItem(items, def);
        }

        private static void printMenuItem(int number, string name) {
            write($"\t{number}", hintColor);
            writeLine($" -> {name}.");
        }

        private static void printMenuTitle(string name) => printTitle(name, 120, titleColor);

        private static void printTitle(string name, int spaseLenght, ConsoleColor color = defTextColor) {
            string it = $"----------{name}";
            for (int i = it.Length; i < spaseLenght; i++) it += "-";
            writeLine();
            writeLine(it, color);
            writeLine();
        }

        private static void writeLine(string value = "", ConsoleColor color = defTextColor) {
            write($"{value}\n", color);
        }

        private static void write(string value = "", ConsoleColor color = defTextColor) {
            Console.ForegroundColor = color;
            Console.Write(value);
            Console.ForegroundColor = defTextColor;
        }
    }
}
